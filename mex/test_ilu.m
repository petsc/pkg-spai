load m1.dat
A=spconvert(m1);
[L,U] = luinc(A,.1);
s=size(A);
b=ones(s(1),1);
[x,flag,relres,iter] = bicgstab(A,b,1e-9,100,L,U)
