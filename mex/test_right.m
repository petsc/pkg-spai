load m1.dat
A=spconvert(m1);
M = spai(A,.4,10);
AM = A*M;
s = size(A);
rhs = A*ones(s(1),1);
[x,flag,relres,iter] = bicgstab(AM,rhs,1e-9,100)
