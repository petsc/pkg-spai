load m1.dat
A=spconvert(m1);
At = transpose(A);
Mt = spai(At);
M = transpose(Mt);
MA = M*A;
s = size(A);
rhs = A*ones(s(1),1);
[x,flag,relres,iter] = bicgstab(MA,M*rhs,1e-9,100)
