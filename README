
SPAI Version 3.0
bug reports and comments to:

   Stephen Barnard
   NASA Ames Research Center, M/S T27A-1 
   Moffett Field, CA 94035-1000 
   barnard@nas.nasa.gov

   or 

   Marcus J. Grote
   Department of Mathematics, ETH Zuurich
   CH-8092 Zurich
   Switzerland
   grote@math.ethz.ch



This directory contains version 3.0 of the SPAI preconditioner.  It
consists of the following subdirectories:

./lib 

Contains code for SPAI and for a simple BICGSTAB routine for
testing. This can be compiled as either a parallel MPI version or a
serial version that doesn't require MPI.

./driver 

Contains a main program for testing SPAI. The executable "spai" will
read a matrix A and (optionally) a rhs b, construct the SPAI
preconditioner M using a combination of various options, and run
BICGSTAB to solver Ax=b.

./mex

Contains an interface to Matlab (supports only the serial version of
SPAI).

./data

An example matrix and rhs.

The makefiles are currently set up for two different architectures: a
single-processor SGI MIPS2 workstation (INDY), and a multiprocessor
SGI Origin2000 (ORIGIN).  Previous versions have run on the IBM SP2,
the Cray T3D, and the CRAY T3E and various other systems. It should be
fairly easy to modify the makefiles for another Unix system. There are
some currently unused conditional-compilation sections in the code
for the SP2 and the T3D (or T3E). See the files qr.c and spai_sub.c.

The GNU gmake utility is required.  If you don't have gmake installed
you can modify the makefiles for whatever make system you have.

---------------------------- Serial version ---------------------------

To make a serial version for an SGI MIPS2 workstation (with the
Matlab interface) do the following:

cd lib
gmake "MACHINE=INDY"

cd ../driver
gmake "MACHINE=INDY"

cd ../mex
build

---------------------------- Serial version ---------------------------

To make a parallel version for an SGI Origin2000 do the following:

cd lib
gmake "MACHINE=ORIGIN"

cd ../driver
gmake "MACHINE=ORIGIN"

----------------------------------------------------------------------

How to Use the Test Program

The test program is called "spai" and is found in the driver
directory. It will: 

   - read a sparse matrix A
   - optionally read a rhs vector b (or construct one)
   - construct an SPAI preconditoning matrix M
   - use BICGSTAB to solve Ax = b, using M as a preconditioner.

The parallel version is executed with:

   mpirun -np <n> ./spai <A> [b] [options]

(Items in <> are required; items in [] are optional.)

The serial program is executed with:

   ./spai <A> [b] [options]

<A> must be a file in Matrix Market coordinate format. See the
file data/m1.mm for an example. (Matrix Market is a repository of
sparse matrices on the web.  The only Matrix Market format currently
supported is "real general". This format is very similar to
the MATLAB sparse matrix format.  See the URL
http://math.nist.gov:80/MatrixMarket/)

[b] is an optional dense vector giving the right-hand-side for the
BICGSTAB solver. It must be a file in Matrix Market array format. See
the file data/m1_rhs.mm for an example. If the rhs is not given then
BICGSTAB will will use A*ones as a rhs, where "ones" is a vector of all
ones.

The solution of the system Ax=b will be written in the file
"solution.mm".

There are a number of optional parameters.  They all have defaults,
and are all of the form "-xx s", where xx is a two-letter abbreviation
of the parameter and s is a string that gives a value for the
parameter. Only a few of them are really important. 

  options: -ep:  epsilon parameter for SPAI
                 default is .4

		 Espilon must be between 0 and 1. It controls the
		 quality of the approximation of M to the inverse of
		 A. Higher values of epsilon lead to more work, more
		 fill, and usually better preconditioners. In many
		 cases the best choice of epsilon is the one that
		 divides the total solution time equally between the
		 preconditioner and the solver.

	   -bs:  block size
	         default is 1

		 This is a new feature in version 3 of SPAI. A block
		 size of 1 treats A as a matrix of scalar elements. A
		 block size of s > 1 treats A as a matrix of sxs
		 blocks. A block size of 0 treats A as a matrix with
		 variable sized blocks, which are determined by
		 searching for dense square diagonal blocks in A.
		 This can be very effective for finite-element
		 matrices.

		 SPAI will convert A to block form, use a block
		 version of the preconditioner algorithm, and then
		 convert the result back to scalar form.

		 In many cases the a block-size parameter other than 1
		 can lead to very significant improvement in
		 performance.

           -ns:  maximum number of improvement steps per row in SPAI
	         default is 5

		 SPAI constructs to approximation to every column of
		 the exact inverse of A in a series of improvement
		 steps. The quality of the approximation is determined
		 by epsilon. If an approximation achieving an accuracy
		 of epsilon is not obtained after ns steps, SPAI simply
		 uses the best approximation constructed so far.

	   -mf:  message file for warning messages
	         default is 0 (/dev/null)

		 Suppose you are using the option "-mf foo". Whenever
		 SPAI fails to achieve an approximation of epsilon for
		 a column of M it writes a message to the file "fooi"
		 where i is the MPI rank of the processor generating
		 the message (or i=0 in the serial case).

	   -mn:  maximum number of new nonzero candidates per step
	         default is 5

	   -sp:  symmetric pattern
	         default is 0

		 If A has a symmetric nonzero pattern use -sp 1 to
		 improve performance by eliminating some communication
		 in the parallel version. Even if A does not have a
		 symmetric nonzero pattern -sp 1 may well lead to good
		 results, but the code will not follow the published
		 SPAI algorithm exactly.

	   -mb:  size of various working buffers in SPAI
	         default is 30000

		 Increase this if you run into problems with crashes.
		 30000 is a very generous size of most problems.

	   -cs:  cache size {0,1,2,3,4,5} in SPAI
	         default is 5 (the biggest cache size)

		 SPAI uses a hash table to cache messages and avoid
		 redundant communication. If suggest always using -cs
		 5. This parameter is irrelevant in the serial
		 version.

	   -mi:  maximum number of iterations in BICGSTAB
	         default is 500

	   -to:  tolerance in BICGSTAB
	         default is 1.0e-10

	   -lp:  left preconditioning
                 default is 0 => right preconditioning

		 SPAI stores matrices in a compressed-column format
		 and by default computes a right preconditioner. If
		 the -lp 1 option is given SPAI will use a
		 compressed-row format and compute a left
		 preconditioner.

	   -bi:  read matrix as a binary file
	         default is 0

		 There is a program called "convert" in the driver
		 directory that converts an ASCII matrix file (in
		 Matrix Market format) to a binary file. This can
		 greatly speed up the input of large matrices.

		 Try:

		 convert ../data/m1.mm m1.binary
		 spai m1.binary -bi 1

	   -vb: verbose
		default is 1

		print parameters, timings and matrix statistics


-------------------------- Matlab Interface ------------------------

Go to the mex directory, execute the script "build", and start Matlab.
Then type "help spai".

The mex directory contains a Matlab version of the m1.mm matrix and a
few sample scripts. Try:

>> test_right_variable_block
>> spy(M)


---------------------------- Hints ----------------------------------

The BICGSTAB routine is only intended for testing.  It is not
efficient in a parallel environment because it uses a sparse
matrix-vector multiply that does not scale well. There will soon be a
PETSc interface for version 3 of SPAI. PETSc is a parallel system of
linear solver and preconditioners developed, maintained, and
distributed by Argonne National Laboratory. (See the URL
http://www-unix.mcs.anl.gov/petsc/.)

You might be puzzled to see that BICGSTAB sometimes produces slightly
different results on different numbers of processors. This is not a
bug. The arithmetic operations are done in different order, and
therefore can lead to different convergence histories. SPAI always
produces the same preconditioning matrix on any number of processors
when the "-bs 1" option is used. Block sizes other than 1 can lead to
slightly different results on different numbers of processors,
however. This is because the scalar-valued matrix is distributed
before blocking is done and the distribution might break up blocks.

To find good SPAI parameters for your application I suggest that you
start with a fairly large value for epsilon, like 0.7, and then
decrease epsilon until you find the best setting. Start the -ns
parameter as something large, like 20, and use the -mf parameter to
see how many columns don't achieve epsilon (if any). It may be most
practical to use a value for -ns that results in some columns not
achieving epsilon. I strongly encourage you to try various -bs
options.

You can write the matrices (either A or M) with a routine called
"write_mm_matrix". Look for some commented-out code in test_spai.c (in
driver) to see how this is done.

This code should work on any parallel system using standard MPI.
However, it is very sensitive to latency. The code and the makefiles
for the "MACHINE=ORIGIN" option are set up to use SGI's SHMEM library
for latency-sensitive operations. This should also work on the T3E,
but it's not tested. See the makefiles to find out how to disable the
SHMEM option. You will have to disable this for other parallel
systems that don't support SHMEM.

Finally, if you are modifiying the code there is a debugging trick
that is very helpful. If you use an option "-db 1" the program will
create n files: dbg0, dbg1, ..., where n is the number of processors.
If you insert code like the following into your program you can print
out information from different processors in these files.

    if (debug) {
      fprintf(fptr_dbg,...);
      fflush(fptr_dbg);
    }

See the file spai.c in lib for an example.







	   



