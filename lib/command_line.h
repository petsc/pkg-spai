/* 
   SPAI Version 3.0 @Copyright 1999,  All Rights Reserved
   Stephen Barnard
*/

#ifndef __command_line_H
#define __command_line_H

#include <string.h>
#include "basics.h"

void parameters
(int, 
 char **,
 char **,
 char **,
 double *,
 int *,
 int *,
 int *,
 int *,
 char **,
 int *,
 double *,
 int *,
 int *,
 int *,
 int *,
 int *,
 int *);

int match_arg
(char *string);

#endif
