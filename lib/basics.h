/* 
   SPAI Version 3.0 @Copyright 1999,  All Rights Reserved
   Stephen Barnard
*/

#ifndef __basics_H
#define __basics_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#ifdef SPAI_USE_MPI
#include "mpi.h"
#define SPAI_Comm MPI_Comm
#else
#define SPAI_Comm void*
#endif

/**********************************************************************/

void init_SPAI();

void basic_distribution
(SPAI_Comm,
 int,
 int,
 int *,
 int *,
 int *,
 int *,
 int *);

#endif
