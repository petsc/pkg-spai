/* 
   SPAI Version 3.0 @Copyright 1999,  All Rights Reserved
   Stephen Barnard
*/

#include "bicgstab.h"

/**********************************************************************/
/* Solves A*x = b
   M is a right approximate inverse of A
   x is an initial guess
   right preconditioner 
*/

void bicgstab_R
(void Av_proc(),
 matrix *A,
 matrix *M,
 vector *x,
 vector *b,
 int maxiter,
 double tol,
 int verbose)
{
  vector *r,*rhat,*p,*v,*w,*z;
  int n,k;
  double c,rho,old_rho,alpha,beta,omega,old_omega;
  double res,res0;

  if (verbose) start_timer(ident_bicgstab);

  if ((A->myid == 0) && verbose) {
    printf("\n\n      bicgstab_R: tol=%e maxiter=%d\n",
	   tol,maxiter);
    printf("iteration     residual\n");
  }

  c = -1.0;
  old_rho = 1.0;
  alpha = 1.0;
  old_omega = 1.0;
  n = A->n;

  p = new_vector(A->n,A->mnl);
  v = new_vector(A->n,A->mnl);
  w = new_vector(A->n,A->mnl);
  z = new_vector(A->n,A->mnl);
  r = new_vector(A->n,A->mnl);
  rhat = new_vector(A->n,A->mnl);

  /* compute initial residual */
  Av_proc(M,x,w);
  Av_proc(A,w,z);
  v_plus_cw(b,z,c,r);
  rcopy_vv(r,rhat);
  res0 = norm(b,A->comm);

  k = 0;
  do {

    k++;

    rho = dot(rhat,r,A->comm);
    beta = (rho/old_rho)*(alpha/old_omega);

    /* compute new p */
    v_plus_cw(p,v,-old_omega,z);
    v_plus_cw(r,z,beta,p);

    /* compute new v, r, and alpha */
    Av_proc(M,p,w);
    Av_proc(A,w,v);

    alpha = rho/dot(rhat,v,A->comm);
    v_plus_cw(r,v,-alpha,w);
    rcopy_vv(w,r);

    /* compute new omega */
    Av_proc(M,r,w);
    Av_proc(A,w,z);

    omega = dot(z,r,A->comm)/dot(z,z,A->comm);

    /* compute new x and new r */
    v_plus_cw(x,p,alpha,w);
    v_plus_cw(w,r,omega,x);
    v_plus_cw(r,z,-omega,w);
    rcopy_vv(w,r);
    old_rho = rho;
    old_omega = omega;

    /* compute exact residual -> w */
    Av_proc(M,x,w);
    Av_proc(A,w,z);
    v_plus_cw(b,z,c,w);
    res = norm(w,A->comm);

    if (!A->myid && verbose) {
      printf("%9d %e\n", k,res/res0);
      fflush(stdout);
    }

  }

  while ((res/res0  > tol) && (k < maxiter));

  /* compute solution M*x */
  Av_proc(M,x,w);
  rcopy_vv(w,x);

  free_vector(p);
  free_vector(v);
  free_vector(w);
  free_vector(z);
  free_vector(r);
  free_vector(rhat);

  if (verbose) {
    stop_timer(ident_bicgstab);
    report_times(ident_bicgstab,"bicgstab",0,A->comm);
  }

}

/**********************************************************************/
/* Solves A*x = b
   M is a left preconditioner
   x is an initial guess
*/

void bicgstab_L
(void Av_proc(),
 matrix *A,
 matrix *M,
 vector *x,
 vector *b,
 int maxiter,
 double tol,
 int verbose)
{
  vector *r,*rhat,*p,*v,*w,*z;
  vector *bb;
  int n,k;
  double c,rho,old_rho,alpha,beta,omega,old_omega;
  double res,res0;

  if (verbose) start_timer(ident_bicgstab);

  if ((A->myid == 0) && verbose) {
    printf("\nbicgstab_L: tol=%e maxiter=%d\n",
	   tol,maxiter);
    printf("iteration residual\n");
  }

  c = -1.0;
  old_rho = 1.0;
  alpha = 1.0;
  old_omega = 1.0;
  n = A->n;
  
  p = new_vector(A->n,A->mnl);
  v = new_vector(A->n,A->mnl);
  w = new_vector(A->n,A->mnl);
  z = new_vector(A->n,A->mnl);
  r = new_vector(A->n,A->mnl);
  rhat = new_vector(A->n,A->mnl);

  bb= new_vector(A->n,A->mnl);

  Av_proc(M,b,bb);

  /* compute initial residual */
  Av_proc(A,x,w);
  Av_proc(M,w,z);

  v_plus_cw(bb,z,c,r);
  rcopy_vv(r,rhat);

  res0 = norm(bb,A->comm);

  k = 0;
  do {

    k++;
    rho = dot(rhat,r,A->comm);
    beta = (rho/old_rho)*(alpha/old_omega);

    /* compute new p */
    v_plus_cw(p,v,-old_omega,z);
    v_plus_cw(r,z,beta,p);

    /* compute new v, r, and alpha */
    Av_proc(A,p,w);
    Av_proc(M,w,v);

    alpha = rho/dot(rhat,v,A->comm);
    v_plus_cw(r,v,-alpha,w);
    rcopy_vv(w,r);

    /* compute new omega */
    Av_proc(A,r,w);
    Av_proc(M,w,z);
    omega = dot(z,r,A->comm)/dot(z,z,A->comm);

    /* compute new x and new r */
    v_plus_cw(x,p,alpha,w);
    v_plus_cw(w,r,omega,x);
    v_plus_cw(r,z,-omega,w);
    rcopy_vv(w,r);
    old_rho = rho;
    old_omega = omega;

    /* compute exact residual -> w */
    Av_proc(A,x,w);
    Av_proc(M,w,z);
    v_plus_cw(bb,z,c,w);
    res = norm(w,A->comm);

    if (!A->myid && verbose) {
      printf("%9d %e\n", k,res/res0);
      fflush(stdout);
    }

  }

  while ((res/res0  > tol) && (k < maxiter));

  free_vector(p);
  free_vector(v);
  free_vector(w);
  free_vector(z);
  free_vector(r);
  free_vector(rhat);

  free_vector(bb);

  if (verbose) {
    stop_timer(ident_bicgstab);
    report_times(ident_bicgstab,"bicgstab",0,A->comm);
  }

}



