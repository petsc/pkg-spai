/* 
   SPAI MPI/C version @Copyright 1996,  All Rights Reserved
   Stephen Barnard
*/

/* ********************************************************************
   ********************************************************************

      Main program illustrating the use of the SPAI preconditioner 

      See README for instructions.

      This program will:
         - read a matrix A and right-hand-side b
	 - compute the SPAI preconditioner M
	 - solve the system Ax = b with BICGSTAB

   ********************************************************************
   ********************************************************************/

#include <string.h>
#include "basics.h"
#include "command_line.h"
#include "vector.h"
#include "index_set.h"
#include "matrix.h"
#include "bicgstab.h"
#include "spai.h"
#include "read_mm_matrix.h"
#include "variable_blocks.h"
#include "timing.h"
#include "debug.h"

main(int argc, char *argv[])
{
  /* SPAI parameters */
  double epsilon_param;
  int    nbsteps_param;
  int    max_param;
  int    maxnew_param;
  int    cache_size_param;
  int    block_size_param;
  int    symmetric_pattern_param;
  int    left_precon_param;
  int    verbose_param;


  /* BICGSTAB parameters */
  int    max_iter_param;
  double tolerance_param;
  int    output  = 2;

  /* Binary file ? */
  int    binary_param;

  matrix *A = NULL;
  matrix *M = NULL;
  vector *x = NULL;
  vector *rhs = NULL;

  char *matrix_file;
  char *rhs_file=NULL;
  char *message_file;
  char message_file_name[1024];
  FILE *fptr_message;
  FILE *fptr_rhs;
  FILE *fptr_x;

  int numprocs,myid;
#ifdef SPAI_USE_MPI
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Barrier(MPI_COMM_WORLD);
#else
#define MPI_COMM_WORLD NULL
  numprocs = 1;
  myid = 0;
#endif

  init_SPAI();

  /* The command-line parameters */
  parameters(argc, 
	     argv,
	     &matrix_file,
	     &rhs_file,
	     &epsilon_param,
	     &nbsteps_param,
	     &max_param,
	     &maxnew_param,
	     &cache_size_param,
	     &message_file,
	     &max_iter_param,
	     &tolerance_param,
	     &block_size_param,
	     &symmetric_pattern_param,
	     &left_precon_param,
	     &binary_param,
	     &verbose_param,
	     &debug,
	     MPI_COMM_WORLD);

  if (debug) {
    sprintf(dbg_file_name,"dbg%d",myid);
    fptr_dbg = fopen(dbg_file_name,"w");
  }

  if (! strlen(message_file)) {
    fptr_message = NULL;
  }
  else {
    if (numprocs > 1) sprintf(message_file_name,"%s%d",message_file,myid);
    else sprintf(message_file_name,"%s",message_file);
    fptr_message = fopen(message_file_name,"w");
  }

  /********************************************************************/
  /********************************************************************/
  /*             Read the matrix A (scalar matrix)                    */
  /********************************************************************/
  /********************************************************************/

  A = read_mm_matrix(matrix_file,
		     1,
		     1,
		     symmetric_pattern_param,
		     left_precon_param,
		     binary_param,
		     verbose_param,
		     MPI_COMM_WORLD);

  /********************************************************************/
  /********************************************************************/
  /*           Read the right-hand side, if any                       */
  /********************************************************************/
  /********************************************************************/

  x = uniform_vector(A->n,A->mnls[myid],1.0);
  if (! rhs_file) {
    if (myid == 0 && verbose_param) {
      printf("Couldn't find rhs file: %s\n",rhs_file);
      printf(" ... Using a rhs of A times all ones.\n");
    }
    rhs = new_vector(A->n,A->mnls[myid]);
    if (! left_precon_param)
      A_times_v_cc(A,x,rhs);
    else
      A_times_v_rc(A,x,rhs);
  }
  else {
    rhs = read_rhs_for_matrix(rhs_file, A);
  }

  /*  This commented-out fragment shows how to print A */
  /* write_matrix_mm(A,"A.mm",left_precon_param); */

  /********************************************************************/
  /********************************************************************/
  /*              Compute the SPAI preconditioner                     */
  /********************************************************************/
  /********************************************************************/

  M = bspai
    (A,
     fptr_message,
     epsilon_param, 
     nbsteps_param, 
     max_param, 
     maxnew_param,
     block_size_param,
     cache_size_param,
     verbose_param);

  /*  This commented-out fragment shows how to print M */
  /* write_matrix_mm(M,"M.mm",left_precon_param); */

  /********************************************************************/
  /********************************************************************/
  /*                         Run bi-cgstab.                           */
  /********************************************************************/
  /********************************************************************/

  /* Solution vector */
  rzeros(x);

  /* Solve for Ax=b (where b is the rhs) using M */

  if (! left_precon_param) {
    /* Right preconditioner */
    bicgstab_R
      (A_times_v_cc,
       A,
       M,
       x,
       rhs,
       max_iter_param,
       tolerance_param,
       verbose_param);
  }

  else {
    /* Left preconditioner */
    bicgstab_L
      (A_times_v_rc,
       A,
       M,
       x,
       rhs,
       max_iter_param,
       tolerance_param,
       verbose_param);
  }

  write_vector_mm(x,"solution.mm",MPI_COMM_WORLD);

#ifdef SPAI_USE_MPI
  MPI_Barrier(MPI_COMM_WORLD);
#endif

  sp_free_matrix(A);
  sp_free_matrix(M);

  free_vector(x);
  free_vector(rhs);

  fclose(fptr_message);

#ifdef SPAI_USE_MPI
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
#endif

}



