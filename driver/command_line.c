/* 
   SPAI MPI/C version @Copyright 1996,  All Rights Reserved
   Stephen Barnard
*/

#include "command_line.h"

/**********************************************************************/
/*
  parameter:     matrix file

  options: -ep:  epsilon parameter for SPAI
                 default is .4

           -ns:  maximum number of improvement steps per row in SPAI
	         default is 5

	   -mb:  size of various working buffers in SPAI
	         default is 30000

	   -mn:  maximum number of new nonzero candidate per step
	         default is 5

	   -cs:  cache size {0,1,2,3,4,5} in SPAI
	         default is 5 (the biggest cache size)

	   -mf:  message file for warning messages
	         default is 0

	   -mi:  maximum number of iterations in BICGSTAB
	         default is 500

	   -to:  tolerance in BICGSTAB
	         default is 1.0e-10

	   -bs:  block size
	         default is 1, variable is 0

	   -sp:  symmetric pattern
	         default is 0

	   -lp:  left preconditioning
                 default is 0 => right preconditioning

	   -bi:  read matrix as a binary file
	         default is 0

	   -vb:  verbose: print parameters, timings and matrix statistics
	         default is 1

	   -db:  debugging level
	         default is 0
	   
*/

void parameters
(int argc, 
 char *argv[],
 char **matrix_file_ptr,
 char **rhs_file_ptr,
 double *ep_ptr,
 int *ns_ptr,
 int *mb_ptr,
 int *mn_ptr,
 int *cs_ptr,
 char **mf_ptr,
 int *mi_ptr,
 double *to_ptr,
 int *bs_ptr,
 int *sp_ptr,
 int *lp_ptr,
 int *bi_ptr,
 int *vb_ptr,
 int *db_ptr,
 MPI_Comm comm)

{
  int k;

  /* defaults */
  static char matrix_file[128];
  static char rhs_file[128];
  static double epsilon = .4;
  static int nsteps = 5;
  static int max_buf = 30000;
  static int max_new = 5;
  static int cache_size = 5;
  static char message_file[128] = "";
  static int max_iter = 500;
  static double tolerance = 1.0e-10;
  static int symmetric_pattern = 0;
  static int block_size = 1;
  static int left_precon = 0;
  static int binary = 0;
  static int verbose = 1;
  static int debug_level = 0;

  static int found_matrix_file = 0;
  static int found_rhs_file = 0;

  int numprocs,myid;
#ifdef SPAI_USE_MPI
  MPI_Comm_size(comm,&numprocs);
  MPI_Comm_rank(comm,&myid);
  MPI_Barrier(comm);
#else
  numprocs = 1;
  myid = 0;
#endif

  k=0;

  /* First look for the matrix_file argument */
  if (argc > 1) {
    if (argv[1][0] != '-') {
      found_matrix_file = 1;
      strcpy(matrix_file,argv[++k]);
      argc--;
    }
  }

  /* Next look for the rhs_file argument */
  if (argc > 1) {
    if (argv[2][0] != '-') {
      found_rhs_file = 1;
      strcpy(rhs_file,argv[++k]);
      argc--;
    }
  }

  while (argc-- > 1) {

    switch (match_arg(argv[++k])) {

    case 0: /* -ep */
      epsilon = strtod(argv[++k],NULL);
      argc--;
      break;

    case 1: /* -ns */
      nsteps = strtod(argv[++k],NULL);
      argc--;
      break;
      
    case 2: /* -mb */
      max_buf = strtod(argv[++k],NULL);
      argc--;
      break;

    case 3: /* -mn */
      max_new = strtod(argv[++k],NULL);
      argc--;
      break;

    case 4: /* -cs */
      cache_size = strtod(argv[++k],NULL);
      argc--;
      break;

    case 5: /* -mf */
      strcpy(message_file,argv[++k]);
      argc--;
      break;
      
    case 6: /* -mi */
      max_iter = strtod(argv[++k],NULL);
      argc--;
      break;

    case 7: /* -to */
      tolerance = strtod(argv[++k],NULL);
      argc--;
      break;

    case 8: /* bs */
      block_size = strtod(argv[++k],NULL);
      argc--;
      break;
      
    case 9: /* sp */
      symmetric_pattern = strtod(argv[++k],NULL);
      argc--;
      break;
      
    case 10: /* lp */
      left_precon = strtod(argv[++k],NULL);
      argc--;
      break;
      
    case 11: /* bi */
      binary = strtod(argv[++k],NULL);
      argc--;
      break;
      
    case 12: /* vb */
      verbose = strtod(argv[++k],NULL);
      argc--;
      break;

    case 13: /* db */
      debug_level = strtod(argv[++k],NULL);
      argc--;
      break;
      
    default:  /* illegal parameter */
      if (myid == 0) {
	fprintf(stderr,"illegal parameter: %s\n",argv[k]);
      }
      argc--;
      break;

    }
  }
  
  if (found_matrix_file) *matrix_file_ptr = matrix_file;
  else *matrix_file_ptr = NULL;

  if (found_rhs_file) *rhs_file_ptr = rhs_file;
  else *rhs_file_ptr = NULL;

  *ep_ptr = epsilon;
  *ns_ptr = nsteps;
  *mb_ptr = max_buf;
  *mn_ptr = max_new;
  *cs_ptr = cache_size;
  *mf_ptr = message_file;
  *mi_ptr = max_iter;
  *to_ptr = tolerance;
  *bs_ptr = block_size;
  *sp_ptr = symmetric_pattern;
  *lp_ptr = left_precon;
  *bi_ptr = binary;
  *vb_ptr = verbose;
  *db_ptr = debug_level;

  if (verbose && (myid == 0)) {
    printf("numprocs                   %d\n",  numprocs);
    printf("matrix file                %s\n",  matrix_file);
    printf("rhs file                   %s\n",  rhs_file);
    printf("epsilon (ep)               %le\n", epsilon);
    printf("nbsteps (ns)               %d\n",  nsteps);
    printf("max (mb)                   %d\n",  max_buf);
    printf("maxnew (mn)                %d\n",  max_new);
    printf("cache size (cs)            %d\n",  cache_size);
    printf("message file (mf)          %s\n",  message_file);
    printf("max_iter (mi)              %d\n",  max_iter);
    printf("tolerance (to)             %le\n", tolerance);
    printf("block size (bs)            %d\n",  block_size);
    printf("symmetric pattern (sp)     %d\n",  symmetric_pattern);
    printf("left preconditioner (lp )  %d\n",  left_precon);
    printf("binary (bi)                %d\n",  binary);
    printf("verbose (vb)               %d\n",  verbose);
    printf("debug (db)                 %d\n",  debug_level);
  }

}

/**********************************************************************/

int match_arg(char *string)
{
  static char *param_table[] = {
    "-ep",
    "-ns",
    "-mb",
    "-mn",
    "-cs",
    "-mf",
    "-mi",
    "-to",
    "-bs",
    "-sp",
    "-lp",
    "-bi",
    "-vb",
    "-db"
  };
  int k;

  k = 0;
  do 
    if (!strcmp(string,param_table[k]))
      return(k);
  while (++k < 14);

  return(-1);
}

/**********************************************************************/

